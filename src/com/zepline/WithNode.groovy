package com.zepline

class WithNode {

  static def parse(def config, def script, def closure) {
    return {
      script.node(config) {
        script.checkout script.scm
        closure()
      }
    }
  }
  
}