package com.zepline

class WithContainer {

  static def parse(def config, def script, def closure) {
    return {
      script.container(config) {
        script.checkout script.scm
        closure()
      }
    }
  }
  
}